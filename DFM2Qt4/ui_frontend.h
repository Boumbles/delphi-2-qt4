/********************************************************************************
** Form generated from reading UI file 'frontend.ui'
**
** Created: Thu 29. Jul 11:23:59 2010
**      by: Qt User Interface Compiler version 4.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRONTEND_H
#define UI_FRONTEND_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "clogger.h"

QT_BEGIN_NAMESPACE

class Ui_frontendClass
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_5;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *guiDfmFileName;
    QToolButton *toolButton;
    QLabel *label_2;
    QLineEdit *guiUiFileName;
    QToolButton *toolButton_2;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QGridLayout *gridLayout_2;
    CLogger *logger;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pbPreview;
    QPushButton *pushButton_2;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *frontendClass)
    {
        if (frontendClass->objectName().isEmpty())
            frontendClass->setObjectName(QString::fromUtf8("frontendClass"));
        frontendClass->resize(594, 323);
        verticalLayout = new QVBoxLayout(frontendClass);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_5 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(frontendClass);
        label->setObjectName(QString::fromUtf8("label"));
        label->setLayoutDirection(Qt::RightToLeft);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        guiDfmFileName = new QLineEdit(frontendClass);
        guiDfmFileName->setObjectName(QString::fromUtf8("guiDfmFileName"));

        gridLayout->addWidget(guiDfmFileName, 0, 1, 1, 1);

        toolButton = new QToolButton(frontendClass);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));

        gridLayout->addWidget(toolButton, 0, 2, 1, 1);

        label_2 = new QLabel(frontendClass);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setLayoutDirection(Qt::RightToLeft);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        guiUiFileName = new QLineEdit(frontendClass);
        guiUiFileName->setObjectName(QString::fromUtf8("guiUiFileName"));

        gridLayout->addWidget(guiUiFileName, 1, 1, 1, 1);

        toolButton_2 = new QToolButton(frontendClass);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));

        gridLayout->addWidget(toolButton_2, 1, 2, 1, 1);


        horizontalLayout_4->addLayout(gridLayout);

        horizontalSpacer_6 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(47, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setSizeConstraint(QLayout::SetNoConstraint);
        logger = new CLogger(frontendClass);
        logger->setObjectName(QString::fromUtf8("logger"));
        logger->setEnabled(true);

        gridLayout_2->addWidget(logger, 0, 0, 1, 1);


        horizontalLayout_2->addLayout(gridLayout_2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(348, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton = new QPushButton(frontendClass);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        pbPreview = new QPushButton(frontendClass);
        pbPreview->setObjectName(QString::fromUtf8("pbPreview"));
        pbPreview->setEnabled(false);

        horizontalLayout->addWidget(pbPreview);

        pushButton_2 = new QPushButton(frontendClass);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalSpacer_2 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_3);


        retranslateUi(frontendClass);
        QObject::connect(pushButton_2, SIGNAL(clicked()), frontendClass, SLOT(close()));
        QObject::connect(pushButton, SIGNAL(clicked()), frontendClass, SLOT(convertForm()));
        QObject::connect(toolButton, SIGNAL(clicked()), frontendClass, SLOT(showDfmFileBrowser()));
        QObject::connect(toolButton_2, SIGNAL(clicked()), frontendClass, SLOT(showUiFileBrowser()));
        QObject::connect(pbPreview, SIGNAL(clicked()), frontendClass, SLOT(showPreview()));

        QMetaObject::connectSlotsByName(frontendClass);
    } // setupUi

    void retranslateUi(QWidget *frontendClass)
    {
        frontendClass->setWindowTitle(QApplication::translate("frontendClass", "Borland DFM to QT4 UI converter", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("frontendClass", "DFM File", 0, QApplication::UnicodeUTF8));
        guiDfmFileName->setText(QApplication::translate("frontendClass", "/Users/robert/storage/development/git repo clone/DFM2QT4-Converter/FormMain.dfm", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("frontendClass", "...", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("frontendClass", "UI File", 0, QApplication::UnicodeUTF8));
        guiUiFileName->setText(QApplication::translate("frontendClass", "/Users/robert/storage/development/git repo clone/DFM2QT4-Converter/FormMain.ui", 0, QApplication::UnicodeUTF8));
        toolButton_2->setText(QApplication::translate("frontendClass", "...", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("frontendClass", "Convert", 0, QApplication::UnicodeUTF8));
        pbPreview->setText(QApplication::translate("frontendClass", "Preview", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("frontendClass", "&Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class frontendClass: public Ui_frontendClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRONTEND_H
