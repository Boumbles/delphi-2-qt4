/*
 * CDfm2GuiTree.cpp
 *
 *  Created on: 11.08.2009
 *      Author: Robert Doering
 *
 */

#include "cdfm2guitree.h"
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <QMessageBox>


CDfm2GuiTree::CDfm2GuiTree()
{
}


CDfm2GuiTree::~CDfm2GuiTree()
{
}


/**
 * @brief Parse givin file into givin xml document.
 *
 * @param filename Name of DFM file to parse.
 * @param guiTree CGuiTreeDomDocument to fill with widget from the file.
 *
 * @return 0 = ok
 * @return -1 = error
 */
int CDfm2GuiTree::parseFile(CGuiTreeDomDocument *guiTree, const char *filename)
{
    // Set domDoc to givin guiTree
    domDoc = guiTree;

    // Set filename.
    if(setDfmFileName(filename))
        return(-1);

    return(parseFile());
}


/**
 * @brief Start parsing file, which filename was set up before.
 * @brief Make sure the domDoc and dfmFileName was set before.
 *
 * @return 0 = ok
 * @return -1 = error
 */
int CDfm2GuiTree::parseFile()
{
    string str;
    string substr;
    string menuitem;
    stringstream sstr;

    CGuiTreeDomElement curDomElm;
    QDomElement tmpDomElm;
    QDomText tmpDomTxt;
    QDomElement rootDomElm;

    string pairKey;
    string pairValue;
    enumDfmValueInfo pairValueInfo;
    int parserReturnCode = 0;
    int rc;  // tmp return code

    // Repare xml document
    rootDomElm = domDoc->createElement("guiRoot");
    domDoc->appendChild(rootDomElm);
    curDomElm = rootDomElm;

    dfmFileStream.open(dfmFileName.data());
    if(!dfmFileStream.is_open())
    {
        logging("Can't open DFM file.");
        return(-1);
    }

    while(getline(dfmFileStream, str))
    {
        lineNum++;
        trimSpaces(str);

        if(isDfmObjectLine(str))
        {
            tmpDomElm = domDoc->createElement("guiObject");
            curDomElm.appendChild(tmpDomElm);
            curDomElm = tmpDomElm;  // set new node as current node

            curDomElm.setDomProperty("name", getDfmObjectName(str).c_str());
            curDomElm.setDomProperty("class", getDfmObjectType(str).c_str());
        }
        else if(isDfmObjectEndLine(str))
        {
            tmpDomElm = curDomElm.parentNode().toElement();
            if(curDomElm == rootDomElm)
            {
                logging(QString("Error: more object closed by \"end\" as opened before. (line %1)").arg(lineNum));
                parserReturnCode = -1;
            }
            else
                curDomElm = tmpDomElm;
        }
        else if(isDfmKeyValuePair(str))
        {
            pairKey = getDfmPairKey(str);
            pairValue = getDfmPairValue(str);

            pairValueInfo = getDfmValueInfo(pairValue);
            switch(pairValueInfo)
            {
            case dviNormal:
                curDomElm.setDomProperty(QString(pairKey.data()), QString(pairValue.data()));
                break;
            case dviBinStart:
                rc = parseBinaryValueLines(domDoc, curDomElm, QString(pairKey.data()), QString(pairValue.data()));
                if(rc != 0)
                    parserReturnCode = -1;
                break;
            case dviPanelItemsStart:
                rc = parsePanelItemsValueLines(domDoc, curDomElm, QString(pairKey.data()), QString(pairValue.data()));
                if(rc != 0)
                    parserReturnCode = -1;
                break;
            case dviStringItemsStart:
                rc = parseStringItemsValueLines(domDoc, curDomElm, QString(pairKey.data()), QString(pairValue.data()));
                if(rc != 0)
                    parserReturnCode = -1;
                break;
            default:
                break;
            }
        }
        else
        {
            logging(QString("Can't understand line %1 with \"%2\".").arg(lineNum).arg(QString(str.data())));
            parserReturnCode = -1;
        }
    }

    // close files
    dfmFileStream.close();

    return(parserReturnCode);
}


/**
 * Parse Lines for an binary key-value-pair.
 *
 * Key and value will be added to the domDocument as property
 * for givin domElm.
 * @example
    Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF006CC}

 * @return 0 ok
 * @return -1 Failed
 */
int CDfm2GuiTree::parseBinaryValueLines(CGuiTreeDomDocument *domDoc, CGuiTreeDomElement &domElm, QString key, QString value)
{
    string line;
    enumDfmValueInfo pairValueInfo;

    while(getline(dfmFileStream, line))
    {
        lineNum++;
        trimSpaces(line);
        pairValueInfo = getDfmValueInfo(line);
        switch(pairValueInfo)
        {
        case dviNormal:
            value += QString(line.data());
            break;
        case dviBinEnd:
            value += QString(line.data());
            value.replace(QRegExp("[{}]"),"");
            // append key-value-pair to dom object
            domElm.setDomProperty(key, value);
            return(0);
            break;
        case dviPanelItemsEnd:
        case dviStringItemsEnd:
        case dviBinStart:
        case dviPanelItemsStart:
        case dviStringItemsStart:
        default:
            logging(QString("Error: Unexpected end of binary value in line %1").arg(lineNum));
            return(-1);
            break;
        }
    }
    logging("Error: Unexpected end of file");
    return(-1);
}


/**
 * Parse Lines for an panel items key-value-pair.
 *
 * Key and value will be added to the domDocument as properties
 * for given domElm.
 * @example
    Panels = <
      item
        Width = 350
      end
      item
        Width = 350
      end
      item
        Width = 160
      end
      item
        Width = 80
      end
      item
        Width = 70
      end>


 * @return 0 ok
 * @return -1 Failed
 */
int CDfm2GuiTree::parsePanelItemsValueLines(CGuiTreeDomDocument *domDoc, CGuiTreeDomElement &domElm, QString key, QString value)
{
    string line;
    enumDfmValueInfo pairValueInfo;
    CGuiTreeDomElement domSubElm;

    // Create child node with key as name
    domSubElm = domDoc->createElement(key);
    domElm.appendChild(domSubElm);

    while(getline(dfmFileStream, line))
    {
        lineNum++;
        trimSpaces(line);
        if(isDfmKeyValuePair(line))
            domSubElm.setDomProperty(QString(getDfmPairKey(line).data()), QString(getDfmPairValue(line).data()));
        else
        {
            pairValueInfo = getDfmValueInfo(line);
            switch(pairValueInfo)
            {
            case dviPanelItemsEnd:
                return(0);
                break;
            case dviNormal: break;  // "item" and "end"
            case dviBinEnd:
            case dviStringItemsEnd:
            case dviBinStart:
            case dviPanelItemsStart:
            case dviStringItemsStart:
            default:
                logging(QString("Error: Unexpected end of panel array in line %1").arg(lineNum));
                return(-1);
                break;
            }
        }
    }
    logging("Error: Unexpected end of file");
    return(-1);
}


/**
 * Parse Lines for an string items key-value-pair.
 *
 * Key and value will be added to the domDocument as properties
 * for given domElm.
 * @example
    Items.Strings = (
        'electronic Chart')
 *
 * @return 0 ok
 * @return -1 Failed
 */
int CDfm2GuiTree::parseStringItemsValueLines(CGuiTreeDomDocument *domDoc, CGuiTreeDomElement &domElm, QString key, QString value)
{
    string line;
    enumDfmValueInfo pairValueInfo;
    CGuiTreeDomElement domSubElm;
    QString item;

    // Create child node with key as name
    domSubElm = domDoc->createElement(key);
    domElm.appendChild(domSubElm);

    while(getline(dfmFileStream, line))
    {
        lineNum++;
        trimSpaces(line);

        pairValueInfo = getDfmValueInfo(line);
        switch(pairValueInfo)
        {
        case dviNormal:
        case dviStringItemsEnd:
            item = QString(line.data());
            item.replace(QRegExp("[()']"),"");
            domSubElm.setDomProperty("item", item);

            if(pairValueInfo == dviStringItemsEnd)
                return(0);
            break;
        case dviBinEnd:
        case dviPanelItemsEnd:
        case dviBinStart:
        case dviPanelItemsStart:
        case dviStringItemsStart:
        default:
            logging(QString("Error: Unexpected end of string array in line %1").arg(lineNum));
            return(-1);
            break;
        }
    }
    logging("Error: Unexpected end of file");
    return(-1);
}


/**
 * Set DFM file name.
 *
 * @return 0 = ok
 * @return -1 = Failed
 */
int CDfm2GuiTree::setDfmFileName(const char *filename)
{
    if(filename == NULL)
        return(-1);

    dfmFileName = filename;

    return(0);
}


bool CDfm2GuiTree::isDfmObjectLine(const string str)
{
    if(str.find("object", 0) != string::npos &&
       str.find(":", 0) != string::npos)
        return(true);
    return(false);
}

bool CDfm2GuiTree::isDfmObjectEndLine(const string str)
{
    string s = str;
    trimSpaces(s);
    if(s.compare("end") == 0) // equal?
        return(true);
    return(false);
}

string CDfm2GuiTree::getDfmObjectName(const string line)
{
    string name = line.substr((line.find_first_of("t", 0)+2), line.length());
    name = name.substr(0, name.find_first_of(":", 0));
    return(name);
}

string CDfm2GuiTree::getDfmObjectType(const string line)
{
    string type = line.substr((line.find_first_of(":", 0)+2), line.length());
    return(type);
}



/**
 *
 * sample: Font.Charset = DEFAULT_CHARSET
 */
bool CDfm2GuiTree::isDfmKeyValuePair(const string line)
{
    size_t pos;
    bool found;

    // look for first non space char (key)
    for(pos=0, found=false; pos<line.length(); pos++)
    {
        if(isspace(line.at(pos)))
            continue;
        found = true;
        break;
    }
    if(!found)
        return(false);

    // look for first space after the key
    for(found=false; pos<line.length(); pos++)
    {
        if(!isspace(line.at(pos)))
            continue;
        found = true;
        break;
    }

    // next char has to be an '=' followed by space follows by non-space
    if(line.length() < pos + 3 ||
       line.at(pos + 1) != '=' ||
       isspace(line.at(pos + 3)))
        return(false);

    return(true);
}

/**
 * Analyze given value from an DFM key-value-pair, maybe it has more value data at
 * the next line of DFM file.
 *
 * @param[in] value String with value information.
 * @return enumDfmValueInfo
 */
CDfm2GuiTree::enumDfmValueInfo CDfm2GuiTree::getDfmValueInfo(const string value)
{
    size_t pos;
    const char spaces[] = " \t\r\n";

    // Search for non-spaces at the end
    pos = value.find_last_not_of(spaces);

    // look for binary data
    if(value[pos] == '}')
        return(dviBinEnd);

    // look for panel items
    if(value[pos] == '>')
        return(dviPanelItemsEnd);

    // look for string items
    if(value[pos] == ')')
        return(dviStringItemsEnd);

    // Search for non-spaces at the beginning
    pos = value.find_first_not_of(spaces);

    // look for binary data
    if(value[pos] == '{')
        return(dviBinStart);

    // look for panel items
    if(value[pos] == '<')
        return(dviPanelItemsStart);

    // look for string items
    if(value[pos] == '(')
        return(dviStringItemsStart);

    return(dviNormal);
}


/**
 *
 * @param[in] line DFM file line like "   Font.Name = 'MS Sans Serif'"
 */
string CDfm2GuiTree::getDfmPairKey(const string line)
{
    string rs;
    size_t pos;

    // look for pos of '=' char
    pos = line.find_first_of('=');
    if(pos == string::npos)
        return(rs);  // no key-value-pair

    // substring
    rs = line.substr(0, pos);

    // Remove white spaces at begin and end.
    trimSpaces(rs);

    return(rs);
}

/**
 *
 * @param[in] line DFM file line like "   Font.Name = 'MS Sans Serif'"
 */
string CDfm2GuiTree::getDfmPairValue(const string line)
{
    string rs;
    size_t pos;

    // look for pos of '=' char
    pos = line.find_first_of('=');
    if(pos == string::npos || pos >= line.length())
        return(rs);  // no key-value-pair

    // substring
    rs = line.substr(pos + 1, line.length() - (pos  + 1));

    // Remove white spaces at begin and end.
    trimSpaces(rs);

    return(rs);
}


/**
 * Remove leading and trailing spaces
 */
void CDfm2GuiTree::trimSpaces(string& strText)
{
    const char spaces[] = " \t\r\n";

    // Search spaces at the beginning
    string::size_type nFirst = strText.find_first_not_of(spaces);
    if(nFirst == string::npos)
    {
        // Only spaces. Empty the string
        strText.erase();
        return;
    }

    if(nFirst != 0) // Some spaces at the beginning. Remove them
        strText.erase(0, nFirst);

    // Search spaces at the end
    string::size_type nLast = strText.find_last_not_of(spaces);
    if(nLast != string::npos) // Some spaces at the end. Remove them
        strText.erase(nLast + 1);
}
