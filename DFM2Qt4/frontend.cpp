/**
 *
 *
 *
 *
 *
 * @todo change frontend to use model-view-style for logging, but dont over do with special log messages (make it possible and viewable, but dont do it)
 * @todo fix: bad conversion of big dfm's
 *
 **/


#include "frontend.h"
#include "cdfm2guitree.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include "cguitree2ui.h"
#include "cuidomdocument.h"
#include "cguitreedomdocument.h"


#define APPNAME "DFM2QT4"
#define ORGNAME "DFM2QT4"

frontend::frontend(QWidget *parent)
    : QWidget(parent),
    mFormPreview(NULL)
{
    ui.setupUi(this);

    loadStettings();

    // setup logger
    mLogModel = new QStandardItemModel(0, 4, this);
    mLogModel->setHeaderData(0, Qt::Horizontal, "Name");
    mLogModel->setHeaderData(1, Qt::Horizontal, "DFM-Type");
    mLogModel->setHeaderData(2, Qt::Horizontal, "UI-Type");
    mLogModel->setHeaderData(3, Qt::Horizontal, "Description");

    /*mLogModel->insertRows(0, 1, QModelIndex());
    item = new QStandardItem();
    item->setText("Moin");

    mLogModel->insertRows(0, 1, QModelIndex());
    mLogModel->setData(mLogModel->index(0, 0, QModelIndex()), "lbWhat");
    mLogModel->setData(mLogModel->index(0, 1, QModelIndex()), "TLabel");
    mLogModel->setData(mLogModel->index(0, 2, QModelIndex()), "QLabel");
    mLogModel->setData(mLogModel->index(0, 3, QModelIndex()), "Failed to create something.");
*/

    //ui.tvLogger->setModel(mLogModel);
    ui.logger->setModel(mLogModel);

    // create qdesigner workbench as preview
    mFormPreview = new FormPreview();

    // @todo implement settings
}


frontend::~frontend()
{
    if(mFormPreview)
        delete(mFormPreview);
    delete(mLogModel);
}


void frontend::convertForm()
{
    int rc;
    CGuiTreeDomDocument *guiTree;
    QString strXmlFileName;

    /*
     * Init.
     */
    // clear output
    //ui.lwDetails->clear();  // clear log
    mLogModel->clear();
    // set xml debug output file
    strXmlFileName = ui.guiDfmFileName->text().replace(".dfm", ".xml");
    // backup settings
    this->storeSettings();
    ui.pbPreview->setEnabled(false);

    /*
    QMessageBox msgBox;
    msgBox.setText("It is working.");
    //msgBox.exec();
    */

    // Alloc mem for GUI tree with standardlized widgets
    // @TODO: describe these standard
    guiTree = new CGuiTreeDomDocument();

    // setup parser
    CDfm2GuiTree *parser = new CDfm2GuiTree();
    connect(parser, SIGNAL(logging(const QString &)), this, SLOT(logText(const QString &)));

    // parse file
    rc = parser->parseFile(guiTree, ui.guiDfmFileName->text().toLatin1().constData());
    if(rc != 0)
    {
        logText("Failed to parse DFM file.");
        delete(parser);
        delete(guiTree);
        return;
    }
    logText("Ends successfully");

    /*
     * Conver guiTree to an QT4 UI file
     */

    // Alloc mem for QT4 UI tree
    CUiDomDocument *qt = new CUiDomDocument();

    // Alloc converter
    CGuiTree2Ui *converter = new CGuiTree2Ui();
    connect(converter, SIGNAL(logging(const QString &)), this, SLOT(logText(const QString &)));

    if(0 != converter->convert(qt, guiTree))
        logText("Failed to convert into UI format");
    else
        logText("Converted successfully");

    dumpDomDoc(qt, ui.guiUiFileName->text().toLatin1().constData());
    dumpDomDoc(guiTree, strXmlFileName.toLocal8Bit());

    // free mem
    delete(parser);
    delete(guiTree);
    delete(qt);
    delete(converter);

    ui.pbPreview->setEnabled(true);
}


/**
 * Write giving text into the frontend.
 */
void frontend::logText(const QString & text)
{
    QStandardItem *logMsg;
    QStandardItem *logLine;
    QList<QStandardItem*> logRow;

    logLine = new QStandardItem();
    logLine->setText("2105");

    logMsg = new QStandardItem();
    logMsg->setText(text);
    //logMsg->setData();
    //logMsg->appendColumn(logLine);

    logRow.append(logMsg);
    logRow.append(logLine);
    mLogModel->appendRow(logRow);
    //mLogModel->setData(mLogModel->index(logMsg->index(), 1, QModelIndex()), "Hallo");

    //ui.lwDetails->addItem(text);
}


/**
 * Show file browser for DFM file.
 *
 */
void frontend::showDfmFileBrowser()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    ".",
                                                    tr("Borland Form (*.dfm)"));
    if(fileName.isEmpty())
        return;

    ui.guiDfmFileName->setText(fileName);
    fileName.replace(".dfm", ".ui");
    ui.guiUiFileName->setText(fileName);
}


/**
 * Show file browser for Ui file.
 *
 */
void frontend::showUiFileBrowser()
{
    logText("show UI");
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    ".",
                                                    tr("QT Form (*.ui)"));
    if(fileName.isEmpty())
        return;

    ui.guiUiFileName->setText(fileName);
}


/**
 * Show detailed information about running process
 */
void frontend::showDetails()
{
    /*QRect rect;
    rect = this->geometry();

    if(ui.lwDetails->isVisible())
    {
        ui.lwDetails->setVisible(false);
        rect.setHeight(rect.height() - ui.lwDetails->height());
        this->setGeometry(rect);
        ui.tbShowLog->setText("+");
    }
    else
    {
        rect.setHeight(rect.height() + 100);
        this->setGeometry(rect);
        ui.lwDetails->setVisible(true);
        ui.tbShowLog->setText("-");
    } */
}


/**
 * Show a preview of converted file.
 * Name of file is UI-Name at GUI.
 */
void frontend::showPreview()
{
    QString fileName;
    QString errorMsg;
    int rc;

    // init.
    fileName = ui.guiUiFileName->text();

    rc = mFormPreview->loadUiFile(fileName, &errorMsg);
    if(rc == 0)
        mFormPreview->show();
    else
        this->logText(errorMsg);
}



/**
 * @brief Write out xml tree of an domDoc object.
 *
 * @param domDoc XML DOM document to write out
 */
void frontend::dumpDomDoc(QDomDocument *domDoc, const char * fileName)
{
    ofstream uiFileStream;
    uiFileStream.open(fileName);
    if(!uiFileStream.is_open())
    {
        logText("Can't open UI file.");
    }
    else
    {
        // output
        uiFileStream << (string) domDoc->toString(1).toUtf8();

        // close files
        uiFileStream.close();
    }
}


/**
 * Load settings.
 *
 **/
void frontend::loadStettings()
{
    QSettings s(ORGNAME, APPNAME);
    ui.guiDfmFileName->setText(s.value("gui/dfmfilename", "").toString());
    ui.guiUiFileName->setText(s.value("gui/uifilename", "").toString());
}



/**
 * Save settings.
 *
 **/
void frontend::storeSettings()
{
    QSettings s(ORGNAME, APPNAME);
    s.setValue("gui/dfmfilename", ui.guiDfmFileName->text());
    s.setValue("gui/uifilename", ui.guiUiFileName->text());
}
