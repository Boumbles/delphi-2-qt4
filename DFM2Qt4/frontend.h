#ifndef FRONTEND_H
#define FRONTEND_H

#include <QtGui/QWidget>
#include <QDomDocument>  // QDomDocument
#include <QStandardItemModel>
#include "ui_frontend.h"
#include "formpreview.h"

class frontend : public QWidget
{
    Q_OBJECT

public:
    frontend(QWidget *parent = 0);
    ~frontend();

public slots:
    void convertForm();
    void logText(const QString & text);
    void showDfmFileBrowser();
    void showUiFileBrowser();
    void showDetails();
    void showPreview();

private:
    Ui::frontendClass ui;
    void dumpDomDoc(QDomDocument *domDoc, const char * fileName);
    void storeSettings();
    void loadStettings();

    QStandardItemModel *mLogModel;
    FormPreview *mFormPreview;

};

#endif // FRONTEND_H
